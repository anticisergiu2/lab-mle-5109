import classes.Calculator;
import exceptions.EquationException;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TestCalculator {
    @Test
    public void test() {
        Calculator calculator =new Calculator();
        try {
            assertEquals(24d,calculator.calculate
                    ("7+7+7+3"),0);
        }
        catch (EquationException e) {
            Assert.fail();
        }
        try {
            assertEquals(-40d,calculator.calculate
                    ("-2 * 30 / 2 - 7 * 2 + 4"),0);
        }
        catch (EquationException e) {
            Assert.fail();
        }
        try {
            assertEquals(-40d,calculator.calculate
                    ("a"),0);
            fail();
        }
        catch (EquationException ignored) {
        }
        try {
            assertEquals(-40d,calculator.calculate
                    ("3+7+1+"),0);
            fail();
        }
        catch (EquationException ignored) {
        }
    }
}
