import classes.Calculator;
import exceptions.EquationException;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Calculator calculator=new Calculator();
        Scanner in = new Scanner(System.in);

        String s = in.nextLine();
        try {
            double value = calculator.calculate(s);
            System.out.println(value);
        }
        catch (EquationException e)
        {
            System.out.println("The eq had an error!");
        }
    }
}
