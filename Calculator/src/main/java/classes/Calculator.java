package classes;

import exceptions.EquationException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class Calculator {

    private double value = 0;
    private static List<String> infixToPostfix(String input) {
        int order;
        List<Character> operationsOrder1= Arrays.asList('*','/');
        List<Character> operationsOrder0= Arrays.asList('+','-');
        StringBuilder postfixElem = new StringBuilder();
        Stack<Character> stack = new Stack<>();
        List<String> postfixArray = new ArrayList<>();
        for (int i = 0; i < input.length(); i++) {
            char ch = input.charAt(i);
            if (operationsOrder1.contains(ch)||operationsOrder0.contains(ch)) {

                if (postfixElem.length() > 0) {
                    postfixArray.add(postfixElem.toString());
                }
                postfixElem = new StringBuilder();
                if (stack.size() <= 0)
                    stack.push(ch);
                else {
                    Character chStack = stack.peek();
                    if (operationsOrder1.contains(chStack))
                        order = 1;
                    else
                        order = 0;
                    if (order == 1) {
                        postfixArray.add(String.valueOf(stack.pop()));
                        i--;
                    } else {
                        if (operationsOrder0.contains(ch)) {
                            postfixArray.add(String.valueOf(stack.pop()));
                        }
                        stack.push(ch);
                    }
                }
            } else {
                postfixElem.append(ch);
            }
        }
        postfixArray.add(postfixElem.toString());
        int len = stack.size();
        for (int j = 0; j < len; j++)
            postfixArray.add(stack.pop().toString());

        return postfixArray;
    }

    private void operation( char operator, double right, double left ) throws EquationException {
        switch ( operator )
        {
            case '+':
                value = left+right;
                break;
            case '-':
                value = left-right;
                break;
            case '/':
                if(right==0)
                    throw new EquationException();
                value = left/right;
                break;
            case '*':
                value = left*right;
                break;
            default:
                break;
        }
    }


    public double calculate(String equation) throws EquationException {
        value=0;
        List<String> operations= Arrays.asList("+", "-", "*","/");
        List<String> postFix= infixToPostfix(equation);
        Stack<Double> stack = new Stack<>();
        stack.push(0d);
        try {
            for (String elem : postFix) {
                if (operations.contains(elem)) {
                    operation(elem.charAt(0), stack.pop(), stack.pop());
                    stack.push(getValue());
                } else {
                    stack.push(Double.parseDouble(elem.trim()));
                }
            }
        }
        catch(NumberFormatException e){
            throw new EquationException();
        }
        return getValue();
    }


    public double getValue() {
        return value;
    }
}
