package com.tora.repos;

import it.unimi.dsi.fastutil.objects.ObjectArraySet;

public class ObjectArraySetRepo<T> implements InMemoryRepository<T> {
    ObjectArraySet<T> objectArraySet;

    public ObjectArraySetRepo() {
        objectArraySet = new ObjectArraySet<>();
    }

    @Override
    public void add(T elem) {
        objectArraySet.add(elem);
    }

    @Override
    public boolean contains(T elem) {
        return objectArraySet.contains(elem);
    }

    @Override
    public void remove(T elem) {
        objectArraySet.remove(elem);
    }

    @Override
    public int size() {
        return objectArraySet.size();
    }
}
