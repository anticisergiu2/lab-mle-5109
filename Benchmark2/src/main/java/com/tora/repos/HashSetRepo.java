package com.tora.repos;

import java.util.HashSet;

public class HashSetRepo<T> implements InMemoryRepository<T>{
    HashSet<T> hashSet;

    public HashSetRepo() {
        hashSet =new HashSet<>();
    }

    @Override
    public void add(T elem) {
        hashSet.add(elem);
    }

    @Override
    public boolean contains(T elem)
    {
        return hashSet.contains(elem);
    }

    @Override
    public void remove(T elem) {
        hashSet.remove(elem);
    }

    @Override
    public int size() {
        return hashSet.size();
    }
}
