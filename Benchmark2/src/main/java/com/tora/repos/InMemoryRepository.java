package com.tora.repos;

public interface InMemoryRepository<T> {
    void add(T elem);
    boolean contains(T elem);
    void remove(T elem);
    int size();
}
