package com.tora.repos;

import org.eclipse.collections.impl.set.mutable.UnifiedSet;

public class UnifiedSetRepo<T> implements InMemoryRepository<T> {
    UnifiedSet<T> unifiedSet;

    public UnifiedSetRepo() {
        unifiedSet =new UnifiedSet<>();
    }

    @Override
    public void add(T elem) {
        unifiedSet.add(elem);
    }

    @Override
    public boolean contains(T elem)
    {
        return unifiedSet.contains(elem);
    }

    @Override
    public void remove(T elem) {
        unifiedSet.remove(elem);
    }
    @Override
    public int size() {
        return unifiedSet.size();
    }
}
