package com.tora.repos;

import java.util.ArrayList;

public class ArrayListRepo<T> implements InMemoryRepository<T>{

    ArrayList<T> arrayList;

    public ArrayListRepo() {
        arrayList=new ArrayList<>();
    }

    @Override
    public void add(T elem) {
        arrayList.add(elem);
    }

    @Override
    public boolean contains(T elem)
    {
        return arrayList.contains(elem);
    }

    @Override
    public void remove(T elem) {
        arrayList.remove(elem);
    }

    @Override
    public int size() {
        return arrayList.size();
    }
}
