package com.tora.repos;

import org.eclipse.collections.impl.bag.mutable.HashBag;

public class HashBagRepo<T> implements InMemoryRepository<T>{
    HashBag<T> hashBag;

    public HashBagRepo() {
        hashBag =new HashBag<>();
    }

    @Override
    public void add(T elem) {
        hashBag.add(elem);
    }

    @Override
    public boolean contains(T elem)
    {
        return hashBag.contains(elem);
    }

    @Override
    public void remove(T elem) {
        hashBag.remove(elem);
    }
    @Override
    public int size() {
        return hashBag.size();
    }
}
