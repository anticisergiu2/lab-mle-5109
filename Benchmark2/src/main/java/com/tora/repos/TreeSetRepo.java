package com.tora.repos;

import java.util.TreeSet;

public class TreeSetRepo<T> implements InMemoryRepository<T> {
    TreeSet<T> treeSet;

    public TreeSetRepo() {
        treeSet =new TreeSet<>();
    }

    @Override
    public void add(T elem) {
        treeSet.add(elem);
    }

    @Override
    public boolean contains(T elem)
    {
        return treeSet.contains(elem);
    }

    @Override
    public void remove(T elem) {
        treeSet.remove(elem);
    }
    @Override
    public int size() {
        return treeSet.size();
    }
}
