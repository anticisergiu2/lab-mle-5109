package com.tora.domain;

import java.util.Objects;

public class Order implements Comparable<Order>{
    int id;
    String description;
    double price;

    public Order(int id, String description, double price) {
        this.id = id;
        this.description = description;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public Order setId(int id) {
        this.id = id;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Order setDescription(String description) {
        this.description = description;
        return this;
    }

    public double getPrice() {
        return price;
    }

    public Order setPrice(double price) {
        this.price = price;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id == order.id;
    }
    public int compareTo(Order order){
        return Integer.compare(order.id, id);
    }
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
