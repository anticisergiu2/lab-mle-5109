package com.tora;

import com.tora.domain.Order;
import com.tora.repos.*;
import org.openjdk.jmh.annotations.*;

import java.util.concurrent.ConcurrentHashMap;

public class MyBenchmark {


    @State(Scope.Thread)
    public static class MyState {
        int number = 100;
        UnifiedSetRepo<Order> unifiedSetRepo = new UnifiedSetRepo<>();
        HashBagRepo<Order> hashBagRepo = new HashBagRepo<>();
        HashSetRepo<Order> hashSetRepo = new HashSetRepo<>();
        ArrayListRepo<Order> arrayListRepo = new ArrayListRepo<>();
        TreeSetRepo<Order> treeSetRepo = new TreeSetRepo<>();
        ObjectArraySetRepo<Order> objectArraySetRepo = new ObjectArraySetRepo<>();
        ConcurrentHashMap<Integer, Order> concurrentHashMap = new ConcurrentHashMap<>();

        public MyState() {
            for (int i = 0; i < number; i++) {
                hashSetRepo.add(new Order(i, "asd", 3.0));
                arrayListRepo.add(new Order(i, "asd", 3.0));
                treeSetRepo.add(new Order(i, "asd", 3.0));
                concurrentHashMap.put(i, new Order(i, "asd", 3.0));
                hashBagRepo.add(new Order(i, "asd", 3.0));
                unifiedSetRepo.add(new Order(i, "asd", 3.0));
                objectArraySetRepo.add(new Order(i, "asd", 3.0));
            }
        }
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 5)
    @Measurement(iterations = 5)
    public void testObjectArraySetadd(MyState state) {
        ObjectArraySetRepo<Order> objectArraySet = new ObjectArraySetRepo<>();
        for (int i = 0; i < state.number; i++)
            objectArraySet.add(new Order(i, "asd", 3.0));
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 5)
    @Measurement(iterations = 5)
    public void testObjectArraySetcontains(MyState state) throws Exception {
        for (int i = 0; i < state.number; i++) {
            boolean asd = state.objectArraySetRepo.contains(new Order(i, "asd", 3.0));
            if (!asd)
                throw new Exception();
        }
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 5)
    @Measurement(iterations = 5)
    public void testObjectArraySetremove(MyState state) throws Exception {
        for (int i = 0; i < state.number; i++) {
            state.objectArraySetRepo.remove(new Order(i, "asd", 3.0));
        }
        if (state.objectArraySetRepo.size() != 0)
            throw new Exception();
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 5)
    @Measurement(iterations = 5)
    public void testUnifiedSetadd(MyState state) {
        UnifiedSetRepo<Order> unifiedSet = new UnifiedSetRepo<>();
        for (int i = 0; i < state.number; i++)
            unifiedSet.add(new Order(i, "asd", 3.0));
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 5)
    @Measurement(iterations = 5)
    public void testUnifiedSetcontains(MyState state) throws Exception {
        for (int i = 0; i < state.number; i++) {
            boolean asd = state.unifiedSetRepo.contains(new Order(i, "asd", 3.0));
            if (!asd)
                throw new Exception();
        }
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 5)
    @Measurement(iterations = 5)
    public void testUnifiedSetremove(MyState state) throws Exception {
        for (int i = 0; i < state.number; i++) {
            state.unifiedSetRepo.remove(new Order(i, "asd", 3.0));
        }
        if (state.unifiedSetRepo.size() != 0)
            throw new Exception();
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 5)
    @Measurement(iterations = 5)
    public void testHashBagadd(MyState state) {
        HashBagRepo<Order> hashBag = new HashBagRepo<>();
        for (int i = 0; i < state.number; i++)
            hashBag.add(new Order(i, "asd", 3.0));
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 5)
    @Measurement(iterations = 5)
    public void testHashBagcontains(MyState state) throws Exception {
        for (int i = 0; i < state.number; i++) {
            boolean asd = state.hashBagRepo.contains(new Order(i, "asd", 3.0));
            if (!asd)
                throw new Exception();
        }
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 5)
    @Measurement(iterations = 5)
    public void testHashBagremove(MyState state) throws Exception {
        for (int i = 0; i < state.number; i++) {
            state.hashBagRepo.remove(new Order(i, "asd", 3.0));
        }
        if (state.hashBagRepo.size() != 0)
            throw new Exception();
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 5)
    @Measurement(iterations = 5)
    public void testConcurrentHashMapadd(MyState state) {
        ConcurrentHashMap<Integer, Order> concurrentHashMap = new ConcurrentHashMap<>();
        for (int i = 0; i < state.number; i++)
            concurrentHashMap.put(i, new Order(i, "asd", 3.0));
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 5)
    @Measurement(iterations = 5)
    public void testConcurrentHashMapcontains(MyState state) throws Exception {
        for (int i = 0; i < state.number; i++) {
            boolean asd = state.concurrentHashMap.contains(new Order(i, "asd", 3.0));
            if (!asd)
                throw new Exception();
        }
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 5)
    @Measurement(iterations = 5)
    public void testConcurrentHashMapremove(MyState state) throws Exception {
        for (int i = 0; i < state.number; i++) {
            state.concurrentHashMap.remove(i);
        }
        if (state.concurrentHashMap.size() != 0)
            throw new Exception();
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 5)
    @Measurement(iterations = 5)
    public void testHashSetadd(MyState state) {
        HashSetRepo<Order> hashSet = new HashSetRepo();
        for (int i = 0; i < state.number; i++)
            hashSet.add(new Order(i, "asd", 3.0));
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 5)
    @Measurement(iterations = 5)
    public void testHashSetcontains(MyState state) throws Exception {
        for (int i = 0; i < state.number; i++) {
            boolean asd = state.hashSetRepo.contains(new Order(i, "asd", 3.0));
            if (!asd)
                throw new Exception();
        }
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 5)
    @Measurement(iterations = 5)
    public void testHashSetremove(MyState state) throws Exception {
        for (int i = 0; i < state.number; i++) {
            state.hashSetRepo.remove(new Order(i, "asd", 3.0));
        }
        if (state.hashSetRepo.size() != 0)
            throw new Exception();
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 5)
    @Measurement(iterations = 5)
    public void testArrayListadd(MyState state) {
        ArrayListRepo<Order> arrayList = new ArrayListRepo<>();
        for (int i = 0; i < state.number; i++)
            arrayList.add(new Order(i, "asd", 3.0));
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 5)
    @Measurement(iterations = 5)
    public void testArrayListcontains(MyState state) throws Exception {
        for (int i = 0; i < state.number; i++) {
            boolean asd = state.arrayListRepo.contains(new Order(i, "asd", 3.0));
            if (!asd)
                throw new Exception();
        }
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 5)
    @Measurement(iterations = 5)
    public void testArrayListremove(MyState state) throws Exception {
        for (int i = 0; i < state.number; i++) {
            state.arrayListRepo.remove(new Order(i, "asd", 3.0));
        }
        if (state.arrayListRepo.size() != 0)
            throw new Exception();
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 5)
    @Measurement(iterations = 5)
    public void testTreeSetadd(MyState state) {
        TreeSetRepo<Order> treeSet = new TreeSetRepo<>();
        for (int i = 0; i < state.number; i++)
            treeSet.add(new Order(i, "asd", 3.0));
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 5)
    @Measurement(iterations = 5)
    public void testTreeSetcontains(MyState state) throws Exception {
        for (int i = 0; i < state.number; i++) {
            boolean asd = state.treeSetRepo.contains(new Order(i, "asd", 3.0));
            if (!asd)
                throw new Exception();
        }
    }

    @Benchmark
    @Fork(value = 1, warmups = 1)
    @Warmup(iterations = 5)
    @Measurement(iterations = 5)
    public void testTreeSetremove(MyState state) throws Exception {
        for (int i = 0; i < state.number; i++) {
            state.treeSetRepo.remove(new Order(i, "asd", 3.0));
        }
        if (state.treeSetRepo.size() != 0)
            throw new Exception();
    }
}
