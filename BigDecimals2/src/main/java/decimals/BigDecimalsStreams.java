package decimals;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class BigDecimalsStreams {

    ArrayList<BigDecimal> decimals;

    public BigDecimalsStreams() {
        decimals = new ArrayList<>();
    }

    public void readDecimals(File myObj) {
        Scanner myReader = null;
        try {
            myReader = new Scanner(myObj);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (myReader.hasNextLine()) {
            String data = myReader.nextLine();
            decimals.add(new BigDecimal(data));
        }
    }

    public void addDecimal(BigDecimal bigDecimal) {
        decimals.add(bigDecimal);
    }

    public BigDecimal sum() {
        return decimals.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public BigDecimal average() {
        if (decimals.size() == 0)
            return new BigDecimal(0);
        BigDecimal bigDecimal = decimals.stream().reduce
                (BigDecimal.ZERO, BigDecimal::add);
        return bigDecimal.divide(new BigDecimal(decimals.size()), RoundingMode.HALF_EVEN);
    }

    public void printBiggest() {
        int number = (decimals.size() + 9) / 10;
        decimals.stream().sorted(Comparator.reverseOrder()).limit(number).forEach(System.out::println);
    }

    public List<BigDecimal> getBiggest() {
        int number = (decimals.size() + 9) / 10;
        return decimals.stream().sorted(Comparator.reverseOrder()).limit(number).collect(Collectors.toList());
    }


    public int size() {
        return decimals.size();
    }


    public void serialize(File myObj) {
        try {
            FileOutputStream fos = new FileOutputStream(myObj);
            ObjectOutputStream fout = new ObjectOutputStream(fos);
            for (var x : decimals) {
                fout.writeObject(x);
            }
            fout.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<BigDecimal> deserialize(File myObj, int number) {
        List<BigDecimal> bigDecimals = new ArrayList<>();
        try {
            FileInputStream fin = new FileInputStream(myObj);
            ObjectInputStream oin = new ObjectInputStream(fin);
            for (int i = 0; i < number; i++)
                bigDecimals.add((BigDecimal) oin.readObject());
            oin.close();
            fin.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return bigDecimals;
    }
}
