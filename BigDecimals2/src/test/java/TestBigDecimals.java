import decimals.BigDecimalsStreams;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestBigDecimals {

    BigDecimalsStreams bigDecimalsStreams;

    @Before
    public void before() {
        bigDecimalsStreams = new BigDecimalsStreams();
        bigDecimalsStreams.readDecimals(new File("src/test/test.txt"));
    }

    @Test
    public void testSum() {
        assertEquals(new BigDecimal("59.6"), bigDecimalsStreams.sum());
        bigDecimalsStreams.addDecimal(new BigDecimal("100.2"));
        assertEquals(new BigDecimal("159.8"), bigDecimalsStreams.sum());
    }

    @Test
    public void testAverage() {
        assertEquals(new BigDecimal("6.0"), bigDecimalsStreams.average());
        bigDecimalsStreams.addDecimal(new BigDecimal("100.2"));
        assertEquals(new BigDecimal("14.5"), bigDecimalsStreams.average());
    }

    @Test
    public void testBiggest() {
        assertTrue(bigDecimalsStreams.getBiggest().
                contains(new BigDecimal("10.1")));
        bigDecimalsStreams.addDecimal(new BigDecimal("100.2"));
        assertTrue(bigDecimalsStreams.getBiggest().
                contains(new BigDecimal("100.2")));
    }

    @Test
    public void serializeDeserialize() {
        bigDecimalsStreams.serialize(new File("src/test/testSer.txt"));
        List<BigDecimal> deserialize = bigDecimalsStreams.deserialize(new File("src/test/testSer.txt"), bigDecimalsStreams.size());
        assertEquals(deserialize.size(), bigDecimalsStreams.size());
        
    }
}
